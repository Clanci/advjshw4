const filmUrl = "https://ajax.test-danit.com/api/swapi/films";

fetch(filmUrl, {
    method: "GET"
})
    .then(data => {
        if (!data.ok) {
            throw new Error('Network response was not ok');
        }
        return data.json();
    })
    .then(films => {
        // Chain the promises to ensure charactersInfo is called after displayFilmsList
        return displayFilmsList(films).then(() => films);
    })
    .then(films => {
        return films.reduce((promise, film) => {
            return promise.then(() => charactersInfo(film.characters, film.episodeId));
        }, Promise.resolve());
    })
    .catch(error => {
        console.error("Error", error);
    });

function displayFilmsList(films) {
    const filmList = document.querySelector(".main");
    
        // Check if filmList is available
    if (!filmList) {
        console.error("Film list element not found.");
        return Promise.reject("Film list element not found.");
    }
    
    // Check if film cards are already appended
    const existingFilmCards = filmList.querySelectorAll('.filmCard');
    if (existingFilmCards.length > 0) {
        console.log("Film cards are already appended.");
        return Promise.resolve();
    }
    
    // Define an async function to wait for each film card to be added to the DOM
    const addFilmCards = async () => {
        for (const film of films) {
            await new Promise(resolve => {
                let filmCard = document.createElement("div");
                filmCard.classList.add("filmCard");
                filmCard.setAttribute("film-id", film.episodeId);
                const loader = document.createElement("div");
                loader.classList.add("loader");
    
                let filmCard_title = document.createElement("div")
                filmCard_title.classList.add("filmCard_title");
    
                let episodeTitle = document.createElement("h2");
                episodeTitle.textContent = `Episode ${film.episodeId}: ${film.name}`;
    
                let openingCrawl = document.createElement("p");
                openingCrawl.textContent = film.openingCrawl;
    
                filmCard_title.append(episodeTitle, openingCrawl);
                filmCard.append(filmCard_title, loader);
    
                filmList.append(filmCard);
    
                // Resolve the promise once the film card is added to the DOM
                resolve();
            });
        }
    };
    
        // Return the promise from the async function
        return addFilmCards();
    }

function charactersInfo(charactersUrls, episodeId) {
    let charactersData = [];

    return Promise.all(charactersUrls.map(url => fetch(url).then(response => response.json())))
        .then(characters => {
            characters.forEach(character => {
                charactersData.push(character.name);
            });

            const targetFilm = document.querySelector(`.filmCard[film-id="${episodeId}"]`);

            if (targetFilm) {
                const loaderElement = targetFilm.querySelector('.loader');
                let pElement = document.createElement("p");
                pElement.textContent = "Characters: " + JSON.stringify(charactersData).slice(1, -1).replace(/"/g, ' ');

                loaderElement.remove();
                targetFilm.append(pElement);
            } else {
                console.error(`Film card with film-id="${episodeId}" not found.`);
            }
        })
        .catch(error => {
            console.error("Error fetching characters:", error);
        });
}
